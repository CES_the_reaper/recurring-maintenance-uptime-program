host: hirewell.nowakenterprises.com
user: root
password: spAWrat8etus
folder: /usr/local/src/hirewell/nodeside/
localfolder: nodeside
localupdatefolder: nodesideupdate
[start]
rm -r [localupdatefolder]
mkdir [localupdatefolder]
sshpass -p "[password]" scp -r [user]@[host]:[folder] [localupdatefolder]
rsync --progress -r -u [localfolder] [localupdatefolder]
rsync --progress -r -u [localupdatefolder] [localfolder]
sshpass -p "[password]" scp -r [user]@[localupdatefolder] [host]:[folder]