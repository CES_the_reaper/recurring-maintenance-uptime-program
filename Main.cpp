/*
 * Main.cpp
 *
 *  Created on: Jul 3, 2018
 *      Author: novareaper
 */
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <chrono> // std::chrono::microseconds
#include <thread> // std::this_thread::sleep_for;


using namespace std;
//implementation found from https://bits.mdminhazulhaque.io/cpp/find-and-replace-all-occurrences-in-cpp-string.html for find and replace function
void find_and_replace(string& source, string const& find, string const& replace)
{
    for(string::size_type i = 0; (i = source.find(find, i)) != string::npos;)
    {
        source.replace(i, find.length(), replace);
        i += replace.length();
    }
}
int main(){
	string host,folder,password,user,localfolder,localupdatefolder;
	ifstream ifile;
	std::cout << "Test" << std::endl;
	while(true){
		ifile.open("update.txt");
		if(!ifile){
			cerr << "unable to open update file" << endl;
		}else{
			string line;
			int i = 0;
			while(getline(ifile,line)){
				const int z = i;
				//TODO Fix this from being location based to being dynamic -- though the app shouldn't run if anything is missing and it should alert the user. BEWARE
				switch(z){
					case 0:
						host = line.substr(6);
						break;
					case 1:
						user = line.substr(6);
						break;
					case 2:
						password = line.substr(10);
						break;
					case 3:
						folder = line.substr(8);
						break;
					case 4:
						localfolder = line.substr(13);
						break;
					case 5:
						localupdatefolder = line.substr(19);
						break;
					case 6: break;
					default:
						string command = line;
						find_and_replace(command,"[password]",password.c_str());
						find_and_replace(command,"[user]",user.c_str());
						find_and_replace(command,"[host]",host.c_str());
						find_and_replace(command,"[folder]",folder.c_str());
						find_and_replace(command,"[localfolder]",localfolder.c_str());
						find_and_replace(command,"[localupdatefolder]",localupdatefolder.c_str());
						cout << "Command sent: " << command << endl;
						system(command.c_str());
				}
				i++;
			}
//			std::string command = "scp -r root@hirewell.nowakenterprises.com:/usr/local/src/hirewell/nodeside ~/projects/hirewellnodeside";
//			//std::getline(std::cin,command);
//
//			system(command.c_str());

		}
		ifile.close();
		std::this_thread::sleep_for(std::chrono::seconds(5));
	}
}
